package com.hackathon.skill.servicio;

import com.hackathon.skill.modelo.Colaborador;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ServicioColaborador {
    public Colaborador crearColaborador(Colaborador colaborador);
    public List<Colaborador> obtenerListaColaboradores();
    public Colaborador bajaColaborador(String idColaborador, Colaborador colaboradorBaja);
    public Colaborador actualizaDatosColaborador(String idColaborador, Colaborador colaboradorUpd);
}
