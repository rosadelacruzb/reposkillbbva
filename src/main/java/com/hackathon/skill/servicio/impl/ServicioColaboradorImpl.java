package com.hackathon.skill.servicio.impl;

import com.hackathon.skill.datos.colaborador.RepositorioColaborador;
import com.hackathon.skill.modelo.Colaborador;
import com.hackathon.skill.servicio.ServicioColaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioColaboradorImpl implements ServicioColaborador {

    public ServicioColaboradorImpl() {
    }

    @Autowired
    RepositorioColaborador colaboradorRepository;

    @Override
    public Colaborador crearColaborador(Colaborador colaborador) {
        this.colaboradorRepository.crearColaborador(colaborador);
        return colaborador;
    }

    @Override
    public List<Colaborador> obtenerListaColaboradores() {
        return this.colaboradorRepository.obtenerListaColaboradores();
    }

    @Override
    public Colaborador bajaColaborador(String idColaborador,
                                       Colaborador colaboradorBaja) {
        return this.colaboradorRepository.bajaColaborador(idColaborador, colaboradorBaja);
    }

    @Override
    public Colaborador actualizaDatosColaborador(String idColaborador,
                                                 Colaborador colaboradorUpd) {
        return this.colaboradorRepository.actualizaDatosColaborador(idColaborador, colaboradorUpd);
    }
}
