package com.hackathon.skill.modelo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "skillColaboradors")
public class SkillColaborador {
    String id;
    String DescSkill;
    String nivelConocimiento;
    String certificacion;
    String estado;

    public SkillColaborador(String id, String DescSkill, String nivelConocimiento, String certificacion, String estado) {
        this.id = id;
        this.DescSkill = DescSkill;
        this.nivelConocimiento = nivelConocimiento;
        this.certificacion = certificacion;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String idSkillColaborador) {
        this.id = id;
    }

    public String getDescSkill() {
        return DescSkill;
    }

    public void setDescSkill(String descSkill) {
        this.DescSkill = descSkill;
    }

    public String getNivelConocimiento() {
        return nivelConocimiento;
    }

    public void setNivelConocimiento(String nivelConocimiento) {
        this.nivelConocimiento = nivelConocimiento;
    }

    public String getCertificacion() {
        return certificacion;
    }

    public void setCertificacion(String certificacion) {
        this.certificacion = certificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
