package com.hackathon.skill.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "colaboradores")
public class Colaborador {
    @Id String id;
    String codRegistro;
    String nombres;
    String email;
    String tipo;
    String fabrica;
    String estado;
    List<SkillColaborador> skillColaboradors = new ArrayList<SkillColaborador>();

    public Colaborador(String id, String codRegistro, String nombres, String email, String tipo, String fabrica, String estado) {
        this.id = id;
        this.codRegistro = codRegistro;
        this.nombres = nombres;
        this.email = email;
        this.tipo = tipo;
        this.fabrica = fabrica;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodRegistro() {
        return codRegistro;
    }

    public void setCodRegistro(String codRegistro) {
        this.codRegistro = codRegistro;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public String getFabrica() {
        return fabrica;
    }

    public void setFabrica(String fabrica) {
        this.fabrica = fabrica;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<SkillColaborador> getSkillColaboradors() {
        return skillColaboradors;
    }

    public void setSkillColaboradors(List<SkillColaborador> skillColaboradors) {
        this.skillColaboradors = skillColaboradors;
    }
}
