package com.hackathon.skill.controlador;

import com.hackathon.skill.modelo.Colaborador;
import com.hackathon.skill.servicio.ServicioColaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/apiskill/v1/colaboradores")
public class ControladorColaborador {

    @Autowired
    ServicioColaborador servicioColaborador;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Colaborador agregarColaborador(@RequestBody Colaborador colaboradorAlta) {
        return this.servicioColaborador.crearColaborador(colaboradorAlta);
    }

    @GetMapping
    public List<Colaborador> obtenerListaColaboradores(){
        return this.servicioColaborador.obtenerListaColaboradores();
    }

    @PutMapping("/{idColaborador}")
    public Colaborador actualizaDatosColaborador(@PathVariable String idColaborador,
                                                 @RequestBody Colaborador colaboradorUpd){
        final Colaborador c = this.servicioColaborador.actualizaDatosColaborador(idColaborador, colaboradorUpd);
        if(c == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return c;
    }

    @PatchMapping("/{idColaborador}")
    public Colaborador bajaColaborador(@PathVariable String idColaborador,
                                       @RequestBody Colaborador colaboradorBaja) {
        final Colaborador c = this.servicioColaborador.bajaColaborador(idColaborador, colaboradorBaja);
        if(c == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return c;
    }
}
