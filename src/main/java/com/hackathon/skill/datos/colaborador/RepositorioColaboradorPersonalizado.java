package com.hackathon.skill.datos.colaborador;

import com.hackathon.skill.modelo.Colaborador;

import java.util.List;

public interface RepositorioColaboradorPersonalizado {
    public void crearColaborador(Colaborador colaborador);
    public List<Colaborador> obtenerListaColaboradores();
    public Colaborador bajaColaborador(String id, Colaborador colaboradorBaja);
    public Colaborador actualizaDatosColaborador(String id, Colaborador colaboradorUpd);
}
