package com.hackathon.skill.datos.colaborador;

import com.hackathon.skill.modelo.Colaborador;
import com.hackathon.skill.modelo.SkillColaborador;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RepositorioColaboradorPersonalizadoImpl
    implements RepositorioColaboradorPersonalizado {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void crearColaborador(Colaborador colaborador) {
        final List<SkillColaborador> skillColaboradors = colaborador.getSkillColaboradors();
        skillColaboradors.stream().forEach(sk -> {
            final SkillColaborador sku = this.mongoOperations.save(sk);
            sk.setId(sku.getId());
        });

        colaborador.setSkillColaboradors(null);
        final Colaborador pp = this.mongoOperations.save(colaborador);
        colaborador.setId(pp.getId());
        colaborador.setSkillColaboradors(skillColaboradors);

        Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(colaborador.getId()));

        List<ObjectId> ids_skillColaboradors = skillColaboradors.stream().map(sk -> new ObjectId(sk.getId())).collect(Collectors.toList());
        Update sk = new Update();
        sk.set("ids_skillColaboradors", ids_skillColaboradors);

        this.mongoOperations.updateFirst(q, sk, Colaborador.class);
    }

    @Override
    public List<Colaborador> obtenerListaColaboradores() {
        final LookupOperation join = Aggregation.lookup(
                "skillColaboradors",
                "ids_skillColaboradors",
                "_id",
                "skillColaboradors");
        final AggregationResults<Colaborador> c =
                this.mongoOperations.aggregate(
                        Aggregation.newAggregation(join),
                        Colaborador.class,
                        Colaborador.class);
        return c.getMappedResults();
    }

    @Override
    public Colaborador bajaColaborador(String id, Colaborador colaboradorBaja) {
        final Colaborador c = this.mongoOperations.findById(id, Colaborador.class);
        if(c != null) {
            Query query = new Query(new Criteria("id").is(id));
            Update update = new Update().set("estado", colaboradorBaja.getEstado());
            mongoOperations.updateFirst(query, update, Colaborador.class);
            c.setEstado(colaboradorBaja.getEstado());
            return c;
        }
    //    if(c != null) {
    //        c.setEstado(colaboradorBaja.getEstado());
    //        this.mongoOperations.save(c);
    //        return c;
    //    }
        return null;
    }

    @Override
    public Colaborador actualizaDatosColaborador(String id, Colaborador colaboradorUpd) {
        final Colaborador c = this.mongoOperations.findById(id, Colaborador.class);
        if(c != null) {
            colaboradorUpd.setId(id);
            //System.out.println("  Campo email [" +colaboradorUpd.getEmail()+ "]");
            c.setCodRegistro(((colaboradorUpd.getCodRegistro()) == null ? c.getCodRegistro() :colaboradorUpd.getCodRegistro()));
            c.setNombres((colaboradorUpd.getNombres() == null ? c.getNombres() :colaboradorUpd.getNombres()));
            c.setEmail((colaboradorUpd.getEmail() == null ? c.getEmail() :colaboradorUpd.getEmail()));
            c.setTipo((colaboradorUpd.getTipo() == null ? c.getTipo() :colaboradorUpd.getTipo()));
            c.setFabrica((colaboradorUpd.getFabrica() == null ? c.getFabrica() :colaboradorUpd.getFabrica()));
            c.setSkillColaboradors(colaboradorUpd.getSkillColaboradors());
            Query query = new Query(new Criteria("id").is(id));
            Update update = new Update().set("codRegistro", colaboradorUpd.getCodRegistro()).set("nombres",colaboradorUpd.getNombres()).set("email",colaboradorUpd.getEmail()).set("tipo",colaboradorUpd.getFabrica()).set("fabrica",colaboradorUpd.getFabrica());
            mongoOperations.updateFirst(query, update, Colaborador.class);
            return c;
        }
        return null;

    }
}
