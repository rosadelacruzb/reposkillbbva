package com.hackathon.skill.datos.colaborador;

import com.hackathon.skill.modelo.Colaborador;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioColaborador extends MongoRepository<Colaborador,String>,
        RepositorioColaboradorPersonalizado {
}
