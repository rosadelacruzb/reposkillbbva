package com.hackathon.skill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillBbvaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkillBbvaApplication.class, args);
	}

}
